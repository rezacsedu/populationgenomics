scalaVersion := "2.11.12"

lazy val sparkVersion = "2.2.1"
lazy val adamVersion = "0.23.0"
lazy val h2oVersion = "3.16.0.2"
lazy val sparklingwaterVersion = "2.2.6"

fork := true

//unmanagedClasspath in Runtime += baseDirectory.value / "src" / "main" / "resources"

libraryDependencies ++= Seq(
  "org.apache.spark"        %% "spark-core"               % sparkVersion,
  "org.bdgenomics.adam"     %% "adam-core"                % adamVersion,
  "org.bdgenomics.adam"     %% "adam-apis"                % adamVersion,
  "ai.h2o"                  %% "sparkling-water-core"     % sparklingwaterVersion,
  "ai.h2o"                  %% "sparkling-water-examples" % sparklingwaterVersion,
  "ai.h2o"                  %  "h2o-core"                 % h2oVersion,
  "ai.h2o"                  %  "h2o-algos"                % h2oVersion,
  "ai.h2o"                  %  "h2o-app"                  % h2oVersion,
  "ai.h2o"                  %  "h2o-persist-hdfs"         % h2oVersion,
  "ai.h2o"                  %% "h2o-scala"                % h2oVersion,
  "ai.h2o"                  %  "google-analytics-java"    % "1.1.2-H2O-CUSTOM",
  "joda-time"               % "joda-time"                 % "2.9.9"
)
