Model: Model Metrics Type: Binomial
 Description: Metrics reported on full training frame
 model id: DeepLearning_model_1515517448316_1
 frame id: training
 MSE: 0.19722673
 RMSE: 0.44410217
 AUC: 0.6041667
 logloss: 0.9962115
 mean_per_class_error: 0.5
 default threshold: 0.9929594993591309
 CM: Confusion Matrix (Row labels: Actual class; Column labels: Predicted class):
        FIN  GBR   Error    Rate
   FIN    0    6  1,0000   6 / 6
   GBR    0   24  0,0000  0 / 24
Totals    0   30  0,2000  6 / 30
Gains/Lift Table (Avg response rate: 80,00 %):
  Group  Cumulative Data Fraction  Lower Threshold      Lift  Cumulative Lift  Response Rate  Cumulative Response Rate  Capture Rate  Cumulative Capture Rate         Gain  Cumulative Gain
      1                0,16666667         0,996871  1,250000         1,250000       1,000000                  1,000000      0,208333                 0,208333    25,000000        25,000000
      2                0,16666667         0,995502  0,000000         1,250000       0,000000                  1,000000      0,000000                 0,208333  -100,000000        25,000000
      3                1,00000000         0,992959  0,950000         1,000000       0,760000                  0,800000      0,791667                 1,000000    -5,000000         0,000000
Model Metrics Type: Binomial
 Description: Metrics reported on full validation frame
 model id: DeepLearning_model_1515517448316_1
 frame id: validation
 MSE: 3.763512E-5
 RMSE: 0.0061347466
 AUC: 1.0
 logloss: 0.0058859107
 mean_per_class_error: 0.0
 default threshold: 0.9929594993591309
 CM: Confusion Matrix (Row labels: Actual class; Column labels: Predicted class):
        FIN  GBR   Error    Rate
   FIN    0    0     NaN   0 / 0
   GBR    0   10  0,0000  0 / 10
Totals    0   10  0,0000  0 / 10
Gains/Lift Table (Avg response rate: 100,00 %):
  Group  Cumulative Data Fraction  Lower Threshold      Lift  Cumulative Lift  Response Rate  Cumulative Response Rate  Capture Rate  Cumulative Capture Rate         Gain  Cumulative Gain
      1                0,30000000         0,996871  1,000000         1,000000       1,000000                  1,000000      0,300000                 0,300000     0,000000         0,000000
      2                0,30000000         0,994133  0,000000         1,000000       0,000000                  1,000000      0,000000                 0,300000  -100,000000         0,000000
      3                1,00000000         0,992959  1,000000         1,000000       1,000000                  1,000000      0,700000                 1,000000     0,000000         0,000000
Variable Importances:
   Variable Relative Importance Scaled Importance Percentage
-2129902138            1,000000          1,000000   1,000000
Status of Neuron Layers (predicting Region, 2-class classification, bernoulli distribution, CrossEntropy loss, 132.610 weights/biases, 1,5 MB, 4.800 training samples, mini-batch size 1):
 Layer Units             Type Dropout       L1       L2 Mean Rate Rate RMS Momentum Mean Weight Weight RMS Mean Bias Bias RMS
     1     1            Input  0,00 %                                                                                        
     2   256 RectifierDropout 50,00 % 0,000000 0,000000  0,008135 0,004710 0,000000    0,006525   0,091061  0,498200 0,011983
     3   256 RectifierDropout 50,00 % 0,000000 0,000000  0,012014 0,006488 0,000000   -0,000730   0,061699  0,994361 0,026308
     4   256 RectifierDropout 50,00 % 0,000000 0,000000  0,013050 0,042247 0,000000   -0,001016   0,061650  0,997378 0,006746
     5     2          Softmax         0,000000 0,000000  0,001253 0,000811 0,000000   -0,009981   0,350430 -0,000599 0,000996
Scoring History:
           Timestamp   Duration Training Speed    Epochs Iterations     Samples Training RMSE Training LogLoss Training AUC Training Lift Training Classification Error Validation RMSE Validation LogLoss Validation AUC Validation Lift Validation Classification Error
 2018-01-09 18:04:24  0.000 sec                  0,00000          0    0,000000           NaN              NaN          NaN           NaN                           NaN             NaN                NaN            NaN             NaN                             NaN
 2018-01-09 18:04:25  1.203 sec    686 obs/sec  10,00000          1  300,000000       0,44410          0,99621      0,60417       1,25000                       0,20000         0,00613            0,00589        1,00000         1,00000                         0,00000
 2018-01-09 18:04:26  2.321 sec   3166 obs/sec 160,00000         16 4800,000000       0,40704          0,51624      0,60417       1,25000                       0,20000         0,09448            0,08931        1,00000         1,00000                         0,00000
 2018-01-09 18:04:26  2.405 sec   3065 obs/sec 160,00000         16 4800,000000       0,44410          0,99621      0,60417       1,25000                       0,20000         0,00613            0,00589        1,00000         1,00000                         0,00000
