package com.example.Genomics2

import hex.FrameSplitter
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.h2o.H2OContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql._
import org.bdgenomics.adam.rdd.ADAMContext._
import org.bdgenomics.formats.avro.{Genotype, GenotypeAllele}
import water.Key

import scala.collection.JavaConverters._
import scala.collection.immutable.Range.inclusive
import scala.io.Source
import java.io._

import hex.kmeans.KMeansModel.KMeansParameters
import org.apache.spark.sql.types.{
  IntegerType,
  StringType,
  StructField,
  StructType
}
import water.fvec.Frame

import java.io._

import hex.FrameSplitter
import hex.deeplearning.DeepLearning
import hex.deeplearning.DeepLearningModel.DeepLearningParameters
import hex.deeplearning.DeepLearningModel.DeepLearningParameters.Activation
import org.apache.spark.SparkContext
import org.apache.spark.h2o.H2OContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql._
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

import org.bdgenomics.adam.rdd.ADAMContext._
import org.bdgenomics.formats.avro.{ Genotype, GenotypeAllele }
import water.{ Job, Key }
import water.support.ModelMetricsSupport
import water.fvec.Frame

import org.apache.spark.h2o._
import java.io.File
import water._

import _root_.hex.{ModelMetrics, ModelMetricsUnsupervised}

import scala.collection.JavaConverters._
import scala.collection.immutable.Range.inclusive
import scala.io.Source

object PopStratClusterings {

  def main(args: Array[String]): Unit = {
    val genotypeFile = "C:/Users/admin/Downloads/genotypes.vcf"
    val panelFile = "C:/Users/admin/Downloads/genotypes.panel"

    val sparkSession: SparkSession =
      SparkSession.builder.appName("PopStrat").master("local[*]").getOrCreate()
    val sc: SparkContext = sparkSession.sparkContext

    //val populations = Set("GBR", "ASW", "FIN", "CHB", "CLM")
    val populations = Set(
      "CHB",
      "JPT",
      "CHS",
      "CDX",
      "KHV",
      "CEU",
      "TSI",
      "FIN",
      "GBR",
      "IBS",
      "YRI",
      "LWK",
      "GWD",
      "MSL",
      "ESN",
      "ASW",
      "ACB",
      "MXL",
      "PUR",
      "CLM",
      "PEL",
      "GIH",
      "PJL",
      "BEB",
      "STU",
      "ITU"
    )

    def extract(file: String,
                filter: (String, String) => Boolean): Map[String, String] = {
      Source
        .fromFile(file)
        .getLines()
        .map(line => {
          val tokens = line.split(Array('\t', ' ')).toList
          tokens(0) -> tokens(1)
        })
        .toMap
        .filter(tuple => filter(tuple._1, tuple._2))
    }
    val panel: Map[String, String] = extract(
      panelFile,
      (sampleID: String, pop: String) => populations.contains(pop))
    val allGenotypes: RDD[Genotype] = sc.loadGenotypes(genotypeFile).rdd
    val genotypes: RDD[Genotype] = allGenotypes.filter(genotype => {
      panel.contains(genotype.getSampleId)
    })

    // Convert the Genotype objects to our own SampleVariant objects to try and conserve memory
    case class SampleVariant(sampleId: String,
                             variantId: Int,
                             alternateCount: Int)
    def variantId(genotype: Genotype): String = {
      val name = genotype.getVariant.getContigName
      val start = genotype.getVariant.getStart
      val end = genotype.getVariant.getEnd
      //val alterallele = genotype.getVariant.getAlternateAllele
      //val refallele = genotype.getVariant.getReferenceAllele
      //val strallele = genotype.getVariant.getSvAllele
      s"$name:$start:$end"
    }
    def alternateCount(genotype: Genotype): Int = {
      genotype.getAlleles.asScala.count(_ != GenotypeAllele.REF)
    }
    def toVariant(genotype: Genotype): SampleVariant = {
      // Intern sample IDs as they will be repeated a lot
      new SampleVariant(genotype.getSampleId.intern(),
                        variantId(genotype).hashCode(),
                        alternateCount(genotype))
    }
    val variantsRDD: RDD[SampleVariant] = genotypes.map(toVariant)
    val variantsBySampleId: RDD[(String, Iterable[SampleVariant])] =
      variantsRDD.groupBy(_.sampleId)
    val sampleCount: Long = variantsBySampleId.count()
    println("Found " + sampleCount + " samples")

    val variantsByVariantId: RDD[(Int, Iterable[SampleVariant])] =
      variantsRDD.groupBy(_.variantId).filter {
        case (_, sampleVariants) => sampleVariants.size == sampleCount
      }

    val variantFrequencies: collection.Map[Int, Int] = variantsByVariantId
      .map {
        case (variantId, sampleVariants) =>
          (variantId, sampleVariants.count(_.alternateCount > 0))
      }
      .collectAsMap()

    val permittedRange = inclusive(11, 11)
    val filteredVariantsBySampleId: RDD[(String, Iterable[SampleVariant])] =
      variantsBySampleId.map {
        case (sampleId, sampleVariants) =>
          val filteredSampleVariants = sampleVariants.filter(
            variant =>
              permittedRange.contains(
                variantFrequencies.getOrElse(variant.variantId, -1)))
          (sampleId, filteredSampleVariants)
      }

    val sortedVariantsBySampleId: RDD[(String, Array[SampleVariant])] =
      filteredVariantsBySampleId.map {
        case (sampleId, variants) =>
          (sampleId, variants.toArray.sortBy(_.variantId))
      }

    println(s"Sorted by Sample ID RDD: " + sortedVariantsBySampleId.first())

    val header = StructType(
      Array(StructField("Region", StringType)) ++
        sortedVariantsBySampleId
          .first()
          ._2
          .map(variant => {
            StructField(variant.variantId.toString, IntegerType)
          }))

    val rowRDD: RDD[Row] = sortedVariantsBySampleId.map {
      case (sampleId, sortedVariants) =>
        val region: Array[String] = Array(panel.getOrElse(sampleId, "Unknown"))
        val alternateCounts: Array[Int] = sortedVariants.map(_.alternateCount)
        Row.fromSeq(region ++ alternateCounts)
    }
    //val featureVectors = rowRDD.map(row => { Vectors.dense(row.toSeq.toArray.map({
    //                                                    case s: String => s.toDouble
    //                                                    case l: Long => l.toDouble
    //                                                   case _ => 0.0 })) })

    //val vector = Vectors.dense{kutta.collect}
    //val myRDD = sc.parallelize(Seq(vector))

    // Create the SchemaRDD from the header and rows and convert the SchemaRDD into a H2O dataframe
    val sqlContext = sparkSession.sqlContext
    val schemaRDD = sqlContext.createDataFrame(rowRDD, header)
    val h2oContext = H2OContext.getOrCreate(sparkSession).init()
    import h2oContext.implicits._
    val dataFrame = h2oContext.asH2OFrame(schemaRDD)

    dataFrame.replace(dataFrame.find("Region"), dataFrame.vec("Region").toCategoricalVec()).remove()
    dataFrame.update()

    //val sqlContext2 = new org.apache.spark.sql.SQLContext(sc)
    //val myRDD = asRDD(dataFrame)

    // Split the dataframe into 50% training, 30% test, and 20% validation data
    val frameSplitter = new FrameSplitter(
      dataFrame,
      Array(.8, .1),
      Array("training", "test", "validation").map(Key.make[Frame]),
      null)
    water.H2O.submitTask(frameSplitter)
    val splits = frameSplitter.getResult
    val training = splits(0)
    val test = splits(1)
    val validation = splits(2)
    //validation.toCSV(true, true)

    //////Traing the K-means model
    //val H2OKMTimer = new Timer
    val clusterParams = new KMeansParameters
    clusterParams._estimate_k = true
    clusterParams._train = training._key
    clusterParams._k = 5
    clusterParams._max_iterations = 1000
    //clusterParams._seed = 123456789
    clusterParams._score_each_iteration
    val model = new hex.kmeans.KMeans(clusterParams).trainModel().get
    println(new String(model._output.writeJSON(new AutoBuffer()).buf()))
    //println(model.)

    val predict = model.score(test)('predict)
    
    //val h2oContext2 = new H2OContext(sparkSession.sparkContext).start()
    import h2oContext._
    import h2oContext.implicits._
    
    val predictionsFromModel = asRDD[DoubleHolder](predict).collect.map(_.result.getOrElse(Double.NaN))
    predictionsFromModel.foreach{ value => println(value)}
    //println(my_score)

    // Collect model metrics and evaluate model quality
    val trainMetrics = ModelMetricsSupport.modelMetrics[ModelMetricsUnsupervised](model, test)
    //def ConfusionMatrix(model: DeepLearningModel, fr: Frame) = ModelMetrics.getFromDKV(trainedModel, test).asInstanceOf[ModelMetricsMultinomial].cm()
    //val met = ModelMetrics.getFromDKV(trainedModel, test).asInstanceOf[ModelMetricsMultinomial].cm()
    val met = trainMetrics.cm()
    //println(met.
        
    // Shutdown Spark cluster and H2O
    h2oContext.stop(stopSparkContext = true)
    sparkSession.stop()
  }
}
